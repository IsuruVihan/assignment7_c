/*
    Isuru Harischandra
    Student ID : 202032 (Temporary)

    *This program will reverse a sentence entered by the user
    conditions => [sentence_length < 1000]
*/

# include <stdio.h>

int main() {
    char string[1000];
    int index;
    // get user input
    puts("Enter your sentence : ");
    gets(string);
    // reversing
    for (index = strlen(string)-1; index >= 0; index--) {
        printf("%c", string[index]);
    }
    return 0;
} 0;
}