/*
    Isuru Harischandra
    Student ID : 202032 (Temporary)

    *This program will find the Frequency of a given character in a string given by the user
    *Characters and strings are case sensitive
    *String length should be less or equal to 1000
*/

# include <stdio.h>

int main() {
    char string[1000];
    char character;
    int index;
    int frequency = 0;
    // get user input
    puts("Enter your string : ");
    gets(string);
    puts("Enter your character : ");
    character = getchar();
    // searching
    for (index = 0; index < strlen(string); index++) {
        if (character == string[index]) {
            frequency++;
        }
    }
    printf("Frequency of \"%c\" in \"%s\" is : %d", character, string, frequency);
    return 0;
}